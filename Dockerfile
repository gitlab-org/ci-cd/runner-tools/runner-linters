ARG GOLANG_VERSION
ARG SHELLCHECK_VERSION
ARG HADOLINT_VERSION

FROM hadolint/hadolint:v${HADOLINT_VERSION}-debian as hadolint
FROM koalaman/shellcheck:v${SHELLCHECK_VERSION} as shellcheck
FROM golang:${GOLANG_VERSION} as build

WORKDIR /build

ARG GOLANGLINT_VERSION

# hadolint ignore=DL3003
RUN git config --global advice.detachedHead false && \
    git clone https://github.com/golangci/golangci-lint.git --no-tags --depth 1 -b "v${GOLANGLINT_VERSION}" repo && \
    cd repo && \
    CGO_ENABLED=1 go build -o /build/out/golangci-lint --trimpath -ldflags "-s -w \
        -X main.version=${GOLANGLINT_VERSION} -X main.commit=$(git rev-parse --short HEAD) -X main.date=$(date -u '+%FT%TZ')" \
        ./cmd/golangci-lint/

# hadolint ignore=DL3003
RUN git clone https://gitlab.com/gitlab-org/language-tools/go/linters/goargs.git --no-tags --depth 1 && \
    cd goargs && \
    CGO_ENABLED=1 go build -buildmode=plugin --trimpath -o /build/out/goargs.so plugin/analyzer.go

# hadolint ignore=DL3003
RUN git clone https://gitlab.com/gitlab-org/language-tools/go/linters/linter2cc.git --no-tags --depth 1 linter2cc && \
    cd linter2cc && \
    CGO_ENABLED=0 go build -o /build/out/linter2cc -ldflags "-s -w \
        -X main.version=${GOLANGLINT_VERSION} -X main.commit=$(git rev-parse --short HEAD) -X main.date=$(date -u '+%FT%TZ')" \
        ./cmd/linter2cc

FROM golang:${GOLANG_VERSION}

ARG MARKDOWNLINT_VERSION
ARG YAMLLINT_VERSION_DEBIAN

# Install required dependencies
RUN apt-get update -yq && \
    apt-get install -yq --no-install-recommends make jq yamllint="${YAMLLINT_VERSION_DEBIAN}" && \
    rm -rf /var/lib/apt/lists/*
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
ENV NODE_VERSION=12.6.0
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} && \
    nvm use v${NODE_VERSION} && \
    nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN npm install -g markdownlint-cli@${MARKDOWNLINT_VERSION}

COPY --from=build /build/out/golangci-lint /build/out/linter2cc /usr/bin/
COPY --from=build /build/out/goargs.so /usr/lib/
COPY --from=shellcheck /bin/shellcheck /bin/
COPY --from=hadolint /bin/hadolint /bin/
