# Runner Linters

The project contains configuration of different linters used by the GitLab Runner and
related projects.

The Dockerfiles contain a Golang environment, [linter2cc](https://gitlab.com/gitlab-org/language-tools/go/linters/linter2cc/),
and all the required linters and plugins.
